class ModelDefiner {
    constructor(ORM) {
        this.ORM = ORM;
        this.Sequelize = this.ORM.Sequelize;
    }

    static getModelName() {
    }

    getFields() {
    }

    getOptions() {
    }

    getClassMethods() {
    }

    getInstanceMethods() {
    }

    getInitialDataset() {
    }

    setForeignKeys() {
    }
}

module.exports = ModelDefiner;
