class ModelInitializer {
    constructor(ORM, name) {
        this.ORM = ORM;
        this.name = name;

        this.defined = false;
        this.modelDefiners = [];

        this.fields = {
            id: {
                type: this.ORM.Sequelize.INTEGER,
                autoIncrement: true,
                primaryKey: true
            }
        };
        this.options = {};
        this.classMethods = {};
        this.instanceMethods = {};
        this.initialDataset = [];
    }

    addFields(fields) {
        Object.assign(this.fields, fields);
    }

    addOptions(options) {
        Object.assign(this.options, options);
    }

    addClassMethods(classMethods) {
        Object.assign(this.classMethods, classMethods);
    }

    addInstanceMethods(instanceMethods) {
        Object.assign(this.instanceMethods, instanceMethods);
    }

    addInitialDataset(initialDataset) {
        initialDataset.forEach((e, i) => {
            if (!this.initialDataset[i]) this.initialDataset[i] = {};
            Object.assign(this.initialDataset[i], e);
        });
    }

    addDefiner(ModelDefiner, origin) {
        const inst = new ModelDefiner(this.ORM, origin);
        this.modelDefiners.push(inst);

        const fields = inst.getFields();
        if (fields) this.addFields(fields);

        const options = inst.getOptions();
        if (options) this.addOptions(options);

        const classMethods = inst.getClassMethods();
        if (classMethods) this.addClassMethods(classMethods);

        const instanceMethods = inst.getInstanceMethods();
        if (instanceMethods) this.addInstanceMethods(instanceMethods);

        const initialDataset = inst.getInitialDataset();
        if (initialDataset) this.addInitialDataset(initialDataset);
    }

    define() {
        if (this.defined) return;

        this.model = this.ORM.db.define(this.name, this.fields, this.options);
        Object.entries(this.classMethods).forEach(([name, method]) => {
            this.model[name] = method;
        });
        Object.entries(this.instanceMethods).forEach(([name, method]) => {
            this.model.prototype[name] = method;
        });

        this.defined = true;
    }

    setForeignKeys() {
        this.modelDefiners.forEach((modelDefiner) => modelDefiner.setForeignKeys());
    }

    async initDataset() {
        if (this.initialDataset.length === 0) return;

        try {
            await this.model.bulkCreate(this.initialDataset);
        } catch (err) {
            throw new Error(`Cannot initiate model: ${err}`);
        }
        this.ORM.logger.info(`[ORM] Model ${this.name} has been initiated.`);
    }
}

module.exports = ModelInitializer;
