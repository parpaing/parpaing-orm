const ParpaingMod = require('@parpaing/parpaing').Mod;
const Sequelize = require('sequelize');

const ModelInitializer = require('./ModelInitializer');

class ORM extends ParpaingMod {
    async setup() {
        this.Sequelize = Sequelize;
        this.modelInitializers = new Map();
        this.models = {};

        try {
            this.db = await this.setupConnexion();
        } catch (err) {
            throw new Error(`Unable to connect to the database: ${err}`);
        }

        this.logger.info('[ORM] Database connection has been established successfully.');
    }

    async init() {
        this.defineModels();

        try {
            await this.db.sync({ force: false });
        } catch (err) {
            throw new Error(`Cannot sync database: ${err}`);
        }
        this.logger.verbose('[ORM] Database sync successfully.');

        if (process.env.ORM_INITDATASET) {
            await this.initDataset();
        }

        this.logger.verbose('[ORM] Initialized.');
    }

    async setupConnexion() {
        const dbConfig = this.config.credentials;

        const options = {
            host: dbConfig.hostname,
            dialect: dbConfig.dialect,
            logging: false,
            pool: {
                max: 5,
                min: 0,
                acquire: 30000,
                idle: 10000
            }
        };

        if (this.config.sequelize) {
            Object.assign(options, this.config.sequelize);
        }

        const db = new Sequelize(dbConfig.database, dbConfig.username, dbConfig.password, options);

        try {
            await db.authenticate();
        } catch (err) {
            throw new Error(`Cannot authenticate to the database: ${err}`);
        }

        return db;
    }

    getModel(name) {
        return this.models.find((m) => m.name === name);
    }

    addDefiners(ModelDefiners, origin) {
        Object.values(ModelDefiners).forEach((ModelDefiner) => {
            this.addDefiner(ModelDefiner, origin);
        });
    }

    addDefiner(ModelDefiner, origin) {
        const modelName = ModelDefiner.getModelName();

        let modelInitializer = this.modelInitializers.get(modelName);
        if (!modelInitializer) {
            modelInitializer = new ModelInitializer(this, modelName);
            this.modelInitializers.set(modelName, modelInitializer);
        }

        modelInitializer.addDefiner(ModelDefiner, origin);
    }

    defineModels() {
        this.modelInitializers.forEach((m) => m.define());
        this.modelInitializers.forEach((m) => { this.models[m.name] = m.model; });
        this.modelInitializers.forEach((m) => m.setForeignKeys());
    }

    async initDataset() {
        try {
            const inits = Array.from(this.modelInitializers, ([, m]) => m.initDataset());
            await Promise.all(inits);

            this.logger.info('[ORM] Models initiated successfully.');
        } catch (err) {
            throw new Error(`Cannot initiate models: ${err}`);
        }
    }
}

module.exports = ORM;
