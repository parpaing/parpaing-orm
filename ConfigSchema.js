const joi = require('@hapi/joi');

const ConfigSchema = joi.object().keys({
    credentials: joi.object().keys({
        hostname: joi.string().hostname().required(),
        username: joi.string().required(),
        password: joi.string().allow('').required(),
        database: joi.string().required(),
        dialect: joi.string().valid('mysql', 'mariadb', 'sqlite', 'postgres', 'mssql').required()
    }).required(),
    sequelize: joi.object().unknown()
}).required();

module.exports = ConfigSchema;
