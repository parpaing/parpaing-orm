const ConfigSchema = require('./ConfigSchema');
const ORM = require('./src/ORM');

const ModelDefiner = require('./src/ModelDefiner');

module.exports = {
    ConfigSchema,
    Mod: ORM,
    ModelDefiner
};
